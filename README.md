**Microservice for handling PayU payments.**

*  The application contains a relationship from the project: https://gitlab.com/marek_m71/authcommon for security application.
*  If you want to use the application, first follow the steps described in: https://gitlab.com/marek_m71/authcommon/blob/master/README.md, related to adding dependencies to nexus.
*  Data for nexus are in files: settings.xml and pom.xml, and should be changed according to your nexus settings.

###Starting service in local environment

cd into project root directory

To setup local environment run

``docker-compose up``

It pulls postgres image, runs container and inits "util" database using sql from

``src/main/resources/db_init/scripts/init.sql``

DB starts on:

``localhost:5432/payu``

``user: postgres``

``password: postgres``


application-local.properties contains configuration for local environment.

To use it type:

``mvn clean package``

``mvn spring-boot:run``