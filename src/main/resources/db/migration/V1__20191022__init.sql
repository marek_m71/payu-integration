CREATE TABLE users
(
    user_id                 BIGSERIAL PRIMARY KEY,
    username                varchar(255),
    email                   varchar(255),
    phone                   varchar(255),
    first_name              varchar(255),
    last_name               varchar(255),
    language                varchar(255)
);

CREATE TABLE orders
(
    order_id                uuid PRIMARY KEY,
    payu_order_id           varchar(255),
    notify_url              varchar(255),
    customer_ip             varchar(255),
    merchant_pos_id         varchar(255),
    validity_time           varchar(255),
    description             varchar(255),
    additional_description  varchar(255),
    currency_code           varchar(255),
    total_amount            varchar(255),
    continue_url            varchar(255),
    creation_date_time      timestamp NOT NULL,
    user_id                 BIGSERIAL REFERENCES users (user_id)
);

CREATE TABLE products
(
    product_id              BIGSERIAL PRIMARY KEY,
    name                    varchar(255),
    unit_price              varchar(255),
    quantity                varchar(255),
    order_id                BIGSERIAL REFERENCES orders (order_id)
);

CREATE TABLE refunds
(
    refund_id                BIGSERIAL PRIMARY KEY,
    refund_payu_id           varchar(255),
    ext_refund_id            varchar(255),
    amount                  numeric,
    description             varchar(255),
    currency_code           varchar(255),
    creation_date_time      timestamp NOT NULL,
    status_date_time         timestamp NOT NULL,
    order_id                 BIGSERIAL REFERENCES orders (order_id)
);

CREATE TABLE status
(
    status_id               BIGSERIAL PRIMARY KEY,
    status_code             varchar(255),
    creation_date_time      timestamp NOT NULL,
    order_id                BIGSERIAL REFERENCES orders (order_id),
    refund_id               BIGSERIAL REFERENCES refunds (refund_id)
);