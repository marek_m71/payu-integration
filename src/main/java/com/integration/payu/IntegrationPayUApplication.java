package com.integration.payu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class IntegrationPayUApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegrationPayUApplication.class, args);
    }
}
