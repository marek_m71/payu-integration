package com.integration.payu.service;

import com.integration.payu.exception.OrderNotFoundException;
import com.integration.payu.exception.RefundNotFoundException;
import com.integration.payu.model.db.Order;
import com.integration.payu.model.db.Refund;
import com.integration.payu.model.db.Status;
import com.integration.payu.model.payu.notify.NotifyPayU;
import com.integration.payu.repository.OrderRepository;
import com.integration.payu.repository.RefundRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class NotifyPayUService {

    private final OrderRepository orderRepository;
    private final RefundRepository refundRepository;

    public NotifyPayUService(OrderRepository orderRepository, RefundRepository refundRepository) {
        this.orderRepository = orderRepository;
        this.refundRepository = refundRepository;
    }

    @Transactional
    public void notify(NotifyPayU notifyPayU){
        if(notifyPayU.getOrder() != null) {
            // ORDER STATUS
            Order order = orderRepository.findByPayUOrderId(notifyPayU.getOrder().getOrderId())
                    .orElseThrow(() -> new OrderNotFoundException(notifyPayU.getOrder().getOrderId()));

            Status status = buildStatus(notifyPayU.getOrder().getStatus());
            order.addStatus(status);

            orderRepository.save(order);
        } else if(notifyPayU.getOrderId() != null) {
            // REFUND STATUS
            Refund refund = refundRepository.findByRefundPayuId(notifyPayU.getRefund().getRefundId())
                    .orElseThrow(() -> new RefundNotFoundException(notifyPayU.getRefund().getRefundId()));
            Status status = buildStatus(notifyPayU.getRefund().getStatus());
            refund.addStatus(status);

            refundRepository.save(refund);
        }
    }

    private Status buildStatus(String status) {
        return Status.builder()
                .statusCode(status)
                .creationDateTime(LocalDateTime.now())
                .build();
    }
}
