package com.integration.payu.service;

import com.authcommon.util.MembershipRetriever;
import com.integration.payu.exception.OrderNotFoundException;
import com.integration.payu.exception.UserNotFoundException;
import com.integration.payu.mapper.OrderMapper;
import com.integration.payu.model.db.Order;
import com.integration.payu.model.db.Product;
import com.integration.payu.model.db.Status;
import com.integration.payu.model.db.User;
import com.integration.payu.model.payu.request.OrderCreateRequestBuyerPayU;
import com.integration.payu.model.payu.request.OrderCreateRequestPayU;
import com.integration.payu.model.payu.response.OrderCreateResponsePayU;
import com.integration.payu.model.rest.request.OrderCreateRequest;
import com.integration.payu.model.rest.response.OrderCreateResponse;
import com.integration.payu.model.rest.response.OrderResponse;
import com.integration.payu.model.rest.response.OrderRetrieveResponse;
import com.integration.payu.model.rest.response.StatusResponse;
import com.integration.payu.providers.PayUClient;
import com.integration.payu.providers.PayUClientTmp;
import com.integration.payu.repository.OrderRepository;
import com.integration.payu.repository.UserRepository;
import com.integration.payu.utils.IPUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private static final String NEW = "NEW";
    private final PayUClient payUClient;
    private final AuthService authService;
    private final OrderMapper orderMapper;
    private final PayUClientTmp payUClientTmp;
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final IPUtils ipUtils;
    private final String clientId;
    private final String payuNotifyUrl;

    public OrderService(PayUClient payUClient,
                        AuthService authService,
                        OrderMapper orderMapper,
                        PayUClientTmp payUClientTmp,
                        UserRepository userRepository,
                        OrderRepository orderRepository,
                        IPUtils ipUtils,
                        @Value("${payu.client.id}") String clientId,
                        @Value("${payu.notify.url}") String payuNotifyUrl) {
        this.payUClient = payUClient;
        this.authService = authService;
        this.orderMapper = orderMapper;
        this.payUClientTmp = payUClientTmp;
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
        this.ipUtils = ipUtils;
        this.clientId = clientId;
        this.payuNotifyUrl = payuNotifyUrl;
    }

    @Transactional
    public OrderCreateResponse orderCreate(OrderCreateRequest orderCreateRequest) {
        OrderCreateRequestPayU orderCreateRequestPayU = orderMapper.map(orderCreateRequest);
        orderCreateRequestPayU.setMerchantPosId(clientId);
        orderCreateRequestPayU.setNotifyUrl(payuNotifyUrl);
        orderCreateRequestPayU.setExtOrderId(UUID.randomUUID());
        orderCreateRequestPayU.setCustomerIp(ipUtils.getRequestIP());
        if (StringUtils.isNotBlank(orderCreateRequestPayU.getContinueUrl())) {
            orderCreateRequestPayU.setContinueUrl(orderCreateRequestPayU.getContinueUrl() + orderCreateRequestPayU.getExtOrderId());
        }
        OrderCreateResponsePayU orderCreateResponsePayU = payUClientTmp.orderCreate(orderCreateRequestPayU, authService.getToken());
        saveNewOrder(orderCreateRequestPayU, orderCreateResponsePayU);

        return orderMapper.map(orderCreateResponsePayU);
    }

    @Transactional
    public List<OrderResponse> getOrders() {
        return orderMapper.map(userRepository.findByUsername(MembershipRetriever.getUsername())
                .orElseThrow(() -> new UserNotFoundException(MembershipRetriever.getUsername())).getOrders())
                .stream().peek(r -> {
                    r.setActualStatusCode(getLastStatusCode(r.getStatus()));
                    r.setRefunds(r.getRefunds().stream().peek(z ->
                            z.setActualStatus(getLastStatusCode(z.getStatus())))
                            .collect(Collectors.toList()));
                }).collect(Collectors.toList());
    }

    @Transactional
    public OrderResponse getOrder(UUID orderId) {
        OrderResponse result = orderMapper.map(orderRepository.findById(orderId)
                .orElseThrow(() -> new OrderNotFoundException(String.valueOf(orderId))));
        result.setActualStatusCode(getLastStatusCode(result.getStatus()));
        result.setRefunds(result.getRefunds().stream().peek(z ->
                z.setActualStatus(getLastStatusCode(z.getStatus()))).collect(Collectors.toList()));
        return result;
    }

    @Transactional
    public OrderRetrieveResponse getOrder(String orderId) {
        return orderMapper.map(payUClient.getOrder(orderId, authService.getToken()));
    }

    private void saveNewOrder(OrderCreateRequestPayU orderCreateRequestPayU,
                              OrderCreateResponsePayU orderCreateResponsePayU) {
        Status status = buildStatus(NEW);
        List<Product> productList = buildProductList(orderCreateRequestPayU);

        Order order = buildOrder(orderCreateRequestPayU, orderCreateResponsePayU);
        order.addStatus(status);
        productList.forEach(order::addProduct);

        User user = userRepository.findByUsername(MembershipRetriever.getUsername())
                .orElse(buildUser(orderCreateRequestPayU.getBuyer()));
        user.addOrder(order);

        orderRepository.save(order);
    }

    private List<Product> buildProductList(OrderCreateRequestPayU orderCreateRequestPayU) {
        return orderCreateRequestPayU.getProducts().stream().map(r ->
                Product.builder()
                        .name(r.getName())
                        .quantity(r.getQuantity())
                        .unitPrice(r.getQuantity())
                        .build())
                .collect(Collectors.toList());
    }

    private Status buildStatus(String statusCode) {
        return Status.builder()
                .statusCode(statusCode)
                .creationDateTime(LocalDateTime.now())
                .build();
    }

    private Order buildOrder(OrderCreateRequestPayU orderCreateRequestPayU,
                             OrderCreateResponsePayU orderCreateResponsePayU) {
        return Order.builder()
                .orderId(orderCreateRequestPayU.getExtOrderId())
                .merchantPosId(orderCreateRequestPayU.getMerchantPosId())
                .additionalDescription(orderCreateRequestPayU.getAdditionalDescription())
                .continueUrl(orderCreateRequestPayU.getContinueUrl())
                .currencyCode(orderCreateRequestPayU.getCurrencyCode())
                .customerIp(orderCreateRequestPayU.getCustomerIp())
                .description(orderCreateRequestPayU.getDescription())
                .notifyUrl(orderCreateRequestPayU.getNotifyUrl())
                .totalAmount(orderCreateRequestPayU.getTotalAmount())
                .validityTime(orderCreateRequestPayU.getValidityTime())
                .payUOrderId(orderCreateResponsePayU.getOrderId())
                .products(new ArrayList<>())
                .status(new ArrayList<>())
                .refunds(new ArrayList<>())
                .creationDateTime(LocalDateTime.now())
                .build();
    }

    private User buildUser(OrderCreateRequestBuyerPayU orderCreateRequestBuyerPayU) {
        return User.builder()
                .username(MembershipRetriever.getUsername())
                .email(orderCreateRequestBuyerPayU.getEmail())
                .phone(orderCreateRequestBuyerPayU.getPhone())
                .firstName(orderCreateRequestBuyerPayU.getFirstName())
                .lastName(orderCreateRequestBuyerPayU.getLastName())
                .language(orderCreateRequestBuyerPayU.getLanguage())
                .orders(new ArrayList<>())
                .build();
    }

    private String getLastStatusCode(List<StatusResponse> status) {
        return status.stream().max(Comparator.comparing(StatusResponse::getCreationDateTime))
                .orElse(new StatusResponse()).getStatusCode();
    }
}
