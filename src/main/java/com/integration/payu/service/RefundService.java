package com.integration.payu.service;

import com.integration.payu.exception.OrderNotFoundException;
import com.integration.payu.exception.RefundException;
import com.integration.payu.mapper.RefundMapper;
import com.integration.payu.model.db.Order;
import com.integration.payu.model.db.Refund;
import com.integration.payu.model.db.Status;
import com.integration.payu.model.payu.request.RefundCreateRequestPayU;
import com.integration.payu.model.payu.response.RefundCreateResponsePayU;
import com.integration.payu.model.rest.request.RefundCreateRequest;
import com.integration.payu.providers.PayUClient;
import com.integration.payu.repository.OrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;

@Service
public class RefundService {
    private static final String COMPLETED = "COMPLETED";
    private final OrderRepository orderRepository;
    private final RefundMapper refundMapper;
    private final PayUClient payUClient;
    private final AuthService authService;

    public RefundService(OrderRepository orderRepository, RefundMapper refundMapper, PayUClient payUClient, AuthService authService) {
        this.orderRepository = orderRepository;
        this.refundMapper = refundMapper;
        this.payUClient = payUClient;
        this.authService = authService;
    }

    @Transactional
    public void refund(UUID orderId, RefundCreateRequest refundCreateRequest) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new OrderNotFoundException(String.valueOf(orderId)));

        if (!checkOrder(order))
            throw new RefundException("Order doesn't have status: " + COMPLETED);

        if (!checkAmount(order, refundCreateRequest))
            throw new RefundException("Refund amount is too high!");

        RefundCreateRequestPayU refundCreateRequestPayU = refundMapper.map(refundCreateRequest);
        RefundCreateResponsePayU refundCreateResponsePayU = payUClient.refund(refundCreateRequestPayU,
                order.getPayUOrderId(), authService.getToken());

        saveNewRefund(order, refundCreateRequestPayU, refundCreateResponsePayU);
    }

    private boolean checkOrder(Order order) {
        return COMPLETED.equals(getLastStatusCode(order));
    }

    private boolean checkAmount(Order order, RefundCreateRequest refundCreateRequest) {
        Long sumRefunds = order.getRefunds().stream().map(Refund::getAmount)
                .mapToLong(Long::longValue).sum();
        Long refundAmount = refundCreateRequest.getAmount() != null ?
                refundCreateRequest.getAmount() : Long.parseLong(order.getTotalAmount());
        return Long.parseLong(order.getTotalAmount()) > sumRefunds + refundAmount;
    }

    private void saveNewRefund(Order order,
                               RefundCreateRequestPayU refundCreateRequestPayU,
                               RefundCreateResponsePayU refundCreateResponsePayU) {
        Status status = buildStatus(refundCreateResponsePayU);

        Refund refund = buildRefund(order, refundCreateRequestPayU, refundCreateResponsePayU);
        refund.addStatus(status);
        order.addRefund(refund);

        orderRepository.save(order);
    }

    private Refund buildRefund(Order order,
                               RefundCreateRequestPayU request,
                               RefundCreateResponsePayU response) {
        return Refund.builder()
                .amount(request.getRefund().getAmount() != null ? request.getRefund().getAmount() : Long.parseLong(order.getTotalAmount()))
                .description(response.getRefund().getDescription())
                .currencyCode(response.getRefund().getCurrencyCode())
                .extRefundId(response.getRefund().getExtRefundId())
                .refundPayuId(response.getRefund().getRefundId())
                .creationDateTime(ZonedDateTime.parse(response.getRefund().getCreationDateTime()))
                .statusDateTime(ZonedDateTime.parse(response.getRefund().getStatusDateTime()))
                .status(new ArrayList<>())
                .build();
    }

    private Status buildStatus(RefundCreateResponsePayU refundCreateResponsePayU) {
        return Status.builder()
                .statusCode(refundCreateResponsePayU.getRefund().getStatus())
                .creationDateTime(LocalDateTime.now())
                .build();
    }

    private String getLastStatusCode(Order order) {
        return order.getStatus().stream().max(Comparator.comparing(Status::getCreationDateTime))
                .orElse(new Status()).getStatusCode();
    }
}
