package com.integration.payu.service;

import com.integration.payu.mapper.AuthMapper;
import com.integration.payu.model.rest.response.AuthorizeResponse;
import com.integration.payu.providers.PayUClientAuth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final PayUClientAuth payUClientAuth;
    private final AuthMapper authMapper;
    private final String clientId;
    private final String clientSecret;
    private final String grantType;

    public AuthService(PayUClientAuth payUClientAuth,
                       AuthMapper authMapper,
                       @Value("${payu.client.id}") String clientId,
                       @Value("${payu.client.secret}") String clientSecret,
                       @Value("${payu.grant.type}") String grantType) {
        this.payUClientAuth = payUClientAuth;
        this.authMapper = authMapper;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.grantType = grantType;
    }

    public AuthorizeResponse authorize() {
        return authMapper.map(payUClientAuth.authorize(grantType, clientId, clientSecret));
    }

    String getToken() {
        return "Bearer " + authorize().getAccessToken();
    }
}
