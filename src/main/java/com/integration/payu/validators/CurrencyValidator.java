package com.integration.payu.validators;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CurrencyValidator implements ConstraintValidator<Currency, String> {

    private final static String PLN = "PLN";

    public CurrencyValidator() {
    }

    @Override
    public boolean isValid(String currency, ConstraintValidatorContext context) {
        // TODO only for PLN
        return PLN.equals(currency);
    }
}
