package com.integration.payu.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CurrencyValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Currency {
    String INVALID = "currency is incorrect";

    String message() default INVALID;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
