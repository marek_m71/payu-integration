package com.integration.payu.providers;

import com.integration.payu.model.payu.request.OrderCreateRequestPayU;
import com.integration.payu.model.payu.response.OrderCreateResponsePayU;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PayUClientTmp {
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String ORDER = "/orders";
    private final String api;

    public PayUClientTmp(@Value("${payu.api.url.v2_1}") String api) {
        this.api = api;
    }

    public OrderCreateResponsePayU orderCreate(OrderCreateRequestPayU orderCreateRequestPayU, String token) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set(HEADER_AUTHORIZATION, token);
        HttpEntity<OrderCreateRequestPayU> entity = new HttpEntity<>(orderCreateRequestPayU, headers);

        ResponseEntity<OrderCreateResponsePayU> response = restTemplate.
                exchange(api + ORDER,
                        HttpMethod.POST,
                        entity,
                        OrderCreateResponsePayU.class);

        return response.getBody();
    }
}
