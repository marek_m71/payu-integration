package com.integration.payu.providers;

import com.integration.payu.model.payu.request.OrderCreateRequestPayU;
import com.integration.payu.model.payu.request.RefundCreateRequestPayU;
import com.integration.payu.model.payu.response.OrderCreateResponsePayU;
import com.integration.payu.model.payu.response.OrderRetrieveResponsePayU;
import com.integration.payu.model.payu.response.RefundCreateResponsePayU;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(value = "PayU", url = "${payu.api.url.v2_1}")
public interface PayUClient {

    @PostMapping(value = "/orders", produces = APPLICATION_JSON_VALUE)
    OrderCreateResponsePayU orderCreate(OrderCreateRequestPayU orderCreateRequestPayU,
                                        @RequestHeader("Authorization") String token);

    @GetMapping(value = "/orders/{orderId}", produces = APPLICATION_JSON_VALUE)
    OrderRetrieveResponsePayU getOrder(@PathVariable("orderId") String orderId,
                                       @RequestHeader("Authorization") String token);

    @PostMapping(value = "/orders/{orderId}/refunds")
    RefundCreateResponsePayU refund(RefundCreateRequestPayU refundCreateRequestPayU,
                                    @PathVariable("orderId") String orderId,
                                    @RequestHeader("Authorization") String token);
}