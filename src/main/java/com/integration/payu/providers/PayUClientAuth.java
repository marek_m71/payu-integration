package com.integration.payu.providers;

import com.integration.payu.model.payu.response.AuthorizeResponsePayU;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "PayUAuth", url = "${payu.api.url}")
public interface PayUClientAuth {

    @PostMapping(value = "/pl/standard/user/oauth/authorize", consumes = "application/json")
    AuthorizeResponsePayU authorize(@RequestParam("grant_type") String grantType,
                                    @RequestParam("client_id") String clientId,
                                    @RequestParam("client_secret") String clientSecret);
}
