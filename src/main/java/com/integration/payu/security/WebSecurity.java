package com.integration.payu.security;

import com.authcommon.filter.JWTDecoderFilter;
import com.authcommon.jws.JWSUtils;
import com.authcommon.jwt.JWTProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.authcommon")
public class WebSecurity extends WebSecurityConfigurerAdapter {
    private static final List<String> ALLOWED_ORIGIN_METHODS = Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS");

    private static final String[] AUTH_WHITELIST_SWAGGER = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/documentation",
            "/configuration/ui",
            "/configuration/security",
            "/webjars/**"
    };

    @Value("${jwt.cognito.issuer}")
    private String cognitoIssuer;

    @Value("${jwt.issuer}")
    private String issuer;

    private final JWTProvider cognitoJWTProvider;
    private final JWTProvider customJWTProvicer;
    private final JWSUtils jwsUtils;

    @Autowired
    public WebSecurity(
            @Qualifier("cognito-jwt-provider") final JWTProvider cognitoJWTProvider,
            @Qualifier("jwt-provider") final JWTProvider customJWTProvider,
            final JWSUtils jwsUtils) {

        this.cognitoJWTProvider = cognitoJWTProvider;
        this.customJWTProvicer = customJWTProvider;
        this.jwsUtils = jwsUtils;
    }

    @Override
    public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web) throws Exception {
        web.ignoring().antMatchers(AUTH_WHITELIST_SWAGGER);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        //shouldn't care about CSRF as not browser based
        httpSecurity.cors().and().csrf().disable()
                .antMatcher("/**")
                .addFilterAfter(new JWTDecoderFilter(cognitoJWTProvider, customJWTProvicer, cognitoIssuer, issuer, jwsUtils), BasicAuthenticationFilter.class);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowedMethods(ALLOWED_ORIGIN_METHODS);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
