package com.integration.payu.mapper;

import com.integration.payu.model.payu.response.AuthorizeResponsePayU;
import com.integration.payu.model.rest.response.AuthorizeResponse;
import org.mapstruct.Mapper;

@Mapper
public interface AuthMapper {
    AuthorizeResponse map(AuthorizeResponsePayU in);
}
