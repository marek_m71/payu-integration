package com.integration.payu.mapper;

import com.integration.payu.model.db.Order;
import com.integration.payu.model.db.Product;
import com.integration.payu.model.db.Status;
import com.integration.payu.model.payu.request.OrderCreateRequestBuyerPayU;
import com.integration.payu.model.payu.request.OrderCreateRequestPayU;
import com.integration.payu.model.payu.request.OrderCreateRequestProductPayU;
import com.integration.payu.model.payu.response.*;
import com.integration.payu.model.rest.request.OrderCreateRequest;
import com.integration.payu.model.rest.request.OrderCreateRequestBuyer;
import com.integration.payu.model.rest.request.OrderCreateRequestProduct;
import com.integration.payu.model.rest.response.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = RefundMapper.class)
public interface OrderMapper {
    OrderCreateResponse map(OrderCreateResponsePayU in);

    OrderCreateResponseStatus map(OrderCreateResponseStatusPayU in);

    OrderRetrieveResponse map(OrderRetrieveResponsePayU in);

    OrderRetrieveResponseOrder map(OrderRetrieveResponseOrderPayU in);

    OrderRetrieveResponseProduct map(OrderRetrieveResponseProductPayU in);

    OrderRetrieveResponseStatus map(OrderRetrieveResponseStatusPayU in);

    OrderCreateRequestPayU map(OrderCreateRequest in);

    OrderCreateRequestBuyerPayU map(OrderCreateRequestBuyer in);

    OrderCreateRequestProductPayU map(OrderCreateRequestProduct in);

    OrderResponse map(Order in);

    StatusResponse map(Status in);

    ProductResponse map(Product in);

    List<OrderResponse> map(List<Order> in);
}
