package com.integration.payu.mapper;

import com.integration.payu.model.db.Refund;
import com.integration.payu.model.payu.request.RefundCreateRequestPayU;

import com.integration.payu.model.rest.request.RefundCreateRequest;
import com.integration.payu.model.rest.response.RefundResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(uses = OrderMapper.class)
public interface RefundMapper {
    @Mapping(source = "in", target = "refund")
    RefundCreateRequestPayU map(RefundCreateRequest in);

    RefundResponse map(Refund in);

    List<RefundResponse> map(List<Refund> in);
}