package com.integration.payu.controller;

import com.integration.payu.model.rest.request.OrderCreateRequest;
import com.integration.payu.model.rest.request.RefundCreateRequest;
import com.integration.payu.model.rest.response.OrderCreateResponse;
import com.integration.payu.model.rest.response.OrderResponse;
import com.integration.payu.service.OrderService;
import com.integration.payu.service.RefundService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("orders")
public class OrderController {

    private final OrderService orderService;
    private final RefundService refundService;

    public OrderController(OrderService orderService,
                           RefundService refundService) {
        this.orderService = orderService;
        this.refundService = refundService;
    }

    @PostMapping
    public ResponseEntity<OrderCreateResponse> orderCreate(@RequestHeader("Authorization") String token,
                                                           @Valid @RequestBody OrderCreateRequest orderCreateRequest) {
        return new ResponseEntity<>(orderService.orderCreate(orderCreateRequest), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<OrderResponse>> getOrders(@RequestHeader("Authorization") String token) {
        return new ResponseEntity<>(orderService.getOrders(), HttpStatus.OK);
    }

    @GetMapping("{orderId}")
    public ResponseEntity<OrderResponse> getOrder(@RequestHeader("Authorization") String token,
                                                  @PathVariable("orderId") UUID orderId) {
        return new ResponseEntity<>(orderService.getOrder(orderId), HttpStatus.OK);
    }

    @PostMapping("{orderId}/refund")
    public ResponseEntity refund(@RequestHeader("Authorization") String token,
                                 @PathVariable("orderId") UUID orderId,
                                 @RequestBody RefundCreateRequest refundCreateRequest) {
        refundService.refund(orderId, refundCreateRequest);
        return new ResponseEntity(HttpStatus.OK);
    }
}
