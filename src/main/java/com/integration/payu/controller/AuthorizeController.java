package com.integration.payu.controller;

import com.integration.payu.model.rest.response.AuthorizeResponse;
import com.integration.payu.service.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("auth")
public class AuthorizeController {

    private final AuthService authService;

    public AuthorizeController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping
    public ResponseEntity<AuthorizeResponse> authorize(@RequestHeader("Authorization") String token) {
        return new ResponseEntity<>(authService.authorize(), HttpStatus.OK);
    }
}
