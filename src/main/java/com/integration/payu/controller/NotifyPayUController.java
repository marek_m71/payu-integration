package com.integration.payu.controller;

import com.integration.payu.model.payu.notify.NotifyPayU;
import com.integration.payu.service.NotifyPayUService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/* This controller handles requests sent by PayU */

@RestController
@RequestMapping("/no-auth/payu/notify")
public class NotifyPayUController {

    private final NotifyPayUService notifyPayUService;

    public NotifyPayUController(NotifyPayUService notifyPayUService) {
        this.notifyPayUService = notifyPayUService;
    }

    @PostMapping
    public ResponseEntity notifyPayU(@RequestBody NotifyPayU notifyPayU){
        notifyPayUService.notify(notifyPayU);
        return new ResponseEntity(HttpStatus.OK);
    }
}
