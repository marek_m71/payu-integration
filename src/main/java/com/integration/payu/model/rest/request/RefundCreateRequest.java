package com.integration.payu.model.rest.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
public class RefundCreateRequest {
    @NotBlank
    private String description;
    @Min(value = 1)
    private Long amount;
}
