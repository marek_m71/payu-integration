package com.integration.payu.model.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.integration.payu.validators.Currency;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@Getter
@Setter
public class OrderCreateRequest {
    @NotBlank
    @Size(min = 1, max = 255)
    private String description;
    @NotBlank
    @Currency
    private String currencyCode;
    @NotNull
    @Min(value = 1)
    private Long totalAmount;
    @Min(value = 1)
    private Long validityTime;
    @Size(min = 1, max = 255)
    private String additionalDescription;
    @Pattern(regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]/", message = "continueUrl is incorrect")
    @Size(min = 1, max = 255)
    private String continueUrl;
    @Valid
    @NotNull
    private OrderCreateRequestBuyer buyer;
    @Valid
    @NotNull
    @Size(min = 1)
    private List<OrderCreateRequestProduct> products = null;

    @JsonIgnore
    @AssertTrue(message = "totalAmount is incorrect")
    boolean isTotalAmountIsCorrect(){
        List<OrderCreateRequestProduct> products = this.products;
        Long totalAmount = this.totalAmount;

        long totalProductAmount = 0;

        for (OrderCreateRequestProduct product : products) {
            totalProductAmount = totalProductAmount +
                    (product.getQuantity() * product.getUnitPrice());
        }

        return totalAmount.equals(totalProductAmount);
    }
}
