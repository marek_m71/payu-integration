package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@ToString
public class RefundResponse {
    private Long refundId;
    private String refundPayuId;
    private String extRefundId;
    private Long amount;
    private String currencyCode;
    private String description;
    private ZonedDateTime creationDateTime;
    private ZonedDateTime statusDateTime;
    private String actualStatus;
    private List<StatusResponse> status;
}
