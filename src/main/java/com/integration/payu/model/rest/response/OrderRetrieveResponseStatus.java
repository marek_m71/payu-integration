package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderRetrieveResponseStatus {
    private String statusCode;
    private String statusDesc;
}
