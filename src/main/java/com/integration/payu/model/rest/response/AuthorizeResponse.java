package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorizeResponse {
    private String accessToken;
    private String tokenType;
    private Integer expiresIn;
    private String grantType;
}
