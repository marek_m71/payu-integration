package com.integration.payu.model.rest.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class OrderCreateRequestProduct {
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
    @NotNull
    @Min(value = 1)
    private Long unitPrice;
    @NotNull
    @Min(value = 1)
    private Long quantity;
}
