package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductResponse {
    private Long productId;
    private String name;
    private Long unitPrice;
    private Long quantity;
}
