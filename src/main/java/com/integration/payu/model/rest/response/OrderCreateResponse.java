package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderCreateResponse {
    private String redirectUri;
}
