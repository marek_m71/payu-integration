package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderRetrieveResponse {
    private List<OrderRetrieveResponseOrder> orders = null;
    private OrderRetrieveResponseStatus status;
}
