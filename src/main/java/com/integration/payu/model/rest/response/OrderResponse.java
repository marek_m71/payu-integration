package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class OrderResponse {
    private UUID orderId;
    private String payUOrderId;
    private String notifyUrl;
    private String customerIp;
    private String merchantPosId;
    private String validityTime;
    private String description;
    private String additionalDescription;
    private String currencyCode;
    private Long totalAmount;
    private String continueUrl;
    private String actualStatusCode;
    private LocalDateTime creationDateTime;
    private List<ProductResponse> products;
    private List<StatusResponse> status;
    private List<RefundResponse> refunds;
}
