package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class StatusResponse {
    private Long statusId;
    private String statusCode;
    private LocalDateTime creationDateTime;
}
