package com.integration.payu.model.rest.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class OrderCreateRequestBuyer {
    @NotBlank
    @Email
    private String email;
    @Size(min = 12, max = 12)
    private String phone;
    @Size(min = 1, max = 255)
    private String firstName;
    @Size(min = 1, max = 255)
    private String lastName;
    @Size(min = 1, max = 255)
    private String language;
}
