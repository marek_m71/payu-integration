package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class OrderRetrieveResponseOrder {
    private String orderId;
    private String extOrderId;
    private String orderCreateDate;
    private String notifyUrl;
    private String customerIp;
    private String merchantPosId;
    private String description;
    private String currencyCode;
    private Long totalAmount;
    private String status;
    private List<OrderRetrieveResponseProduct> products = null;
}
