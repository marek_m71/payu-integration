package com.integration.payu.model.rest.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderCreateResponseStatus {
    private String statusCode;
}
