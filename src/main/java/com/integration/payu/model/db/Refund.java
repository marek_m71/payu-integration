package com.integration.payu.model.db;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity(name = "refunds")
public class Refund {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "refund_id")
    private Long refundId;
    @Column(name = "refund_payu_id")
    private String refundPayuId;
    @Column(name = "ext_refund_id")
    private String extRefundId;
    @Column(name = "amount")
    private Long amount;
    @Column(name = "currency_code")
    private String currencyCode;
    @Column(name = "description")
    private String description;
    @Column(name = "creation_date_time")
    private ZonedDateTime creationDateTime;
    @Column(name = "status_date_time")
    private ZonedDateTime statusDateTime;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "refund", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Status> status = new ArrayList<>();
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "order_id")
    private Order order;

    public void addStatus(Status status) {
        status.setRefund(this);
        this.status.add(status);
    }
}
