package com.integration.payu.model.db;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity(name = "orders")
public class Order {
    @Id
    @Column(name = "order_id")
    private UUID orderId;
    @Column(name = "payu_order_id")
    private String payUOrderId;
    @Column(name = "notify_url")
    private String notifyUrl;
    @Column(name = "customer_ip")
    private String customerIp;
    @Column(name = "merchant_pos_id")
    private String merchantPosId;
    @Column(name = "validityTime")
    private String validityTime;
    @Column(name = "description")
    private String description;
    @Column(name = "additionalDescription")
    private String additionalDescription;
    @Column(name = "currency_code")
    private String currencyCode;
    @Column(name = "total_amount")
    private String totalAmount;
    @Column(name = "continue_url")
    private String continueUrl;
    @Column(name = "creation_date_time")
    private LocalDateTime creationDateTime;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id")
    private User user;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Product> products = new ArrayList<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Status> status = new ArrayList<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Refund> refunds = new ArrayList<>();

    public void addStatus(Status status) {
        status.setOrder(this);
        this.status.add(status);
    }

    public void addProduct(Product product) {
        product.setOrder(this);
        this.products.add(product);
    }

    public void addRefund(Refund refund) {
        refund.setOrder(this);
        this.refunds.add(refund);
    }
}
