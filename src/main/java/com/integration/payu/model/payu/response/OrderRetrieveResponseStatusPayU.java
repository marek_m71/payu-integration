package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderRetrieveResponseStatusPayU {
    @JsonProperty("statusCode")
    private String statusCode;
    @JsonProperty("statusDesc")
    private String statusDesc;
}
