package com.integration.payu.model.payu.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RefundCreateRequestInfoTypePayU {
    @JsonProperty("description")
    private String description;
    @JsonProperty("amount")
    private Long amount;
    @JsonProperty("extRefundId")
    private String exdRefundId;
    @JsonProperty("bankDescription")
    private String bankDescription;
    @JsonProperty("type")
    private String type;
}
