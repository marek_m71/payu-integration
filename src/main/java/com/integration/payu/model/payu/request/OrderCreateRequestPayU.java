package com.integration.payu.model.payu.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class OrderCreateRequestPayU {
    @JsonProperty("extOrderId")
    private UUID extOrderId;
    @JsonProperty("notifyUrl")
    private String notifyUrl;
    @JsonProperty("customerIp")
    private String customerIp;
    @JsonProperty("merchantPosId")
    private String merchantPosId;
    @JsonProperty("validityTime")
    private String validityTime;
    @JsonProperty("description")
    private String description;
    @JsonProperty("additionalDescription")
    private String additionalDescription;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("totalAmount")
    private String totalAmount;
    @JsonProperty("continueUrl")
    private String continueUrl;
    @JsonProperty("buyer")
    private OrderCreateRequestBuyerPayU buyer;
    @JsonProperty("products")
    private List<OrderCreateRequestProductPayU> products = null;
}
