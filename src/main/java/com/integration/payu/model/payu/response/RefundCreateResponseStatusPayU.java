package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RefundCreateResponseStatusPayU {
    @JsonProperty("statusCode")
    private String statusCode;
    @JsonProperty("statusDesc")
    private String statusDesc;
}
