package com.integration.payu.model.payu.notify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class NotifyPayU {
    @JsonProperty("order")
    private NotifyOrderPayU order;
    @JsonProperty("localReceiptDateTime")
    private String localReceiptDateTime;
    @JsonProperty("properties")
    private List<NotifyPropertyPayU> properties = null;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("extOrderId")
    private String extOrderId;
    @JsonProperty("refund")
    private NotifyRefundPayU refund;
}
