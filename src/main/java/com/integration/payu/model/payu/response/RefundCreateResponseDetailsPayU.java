package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RefundCreateResponseDetailsPayU {
    @JsonProperty("refundId")
    private String refundId;
    @JsonProperty("extRefundId")
    private String extRefundId;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("description")
    private String description;
    @JsonProperty("creationDateTime")
    private String creationDateTime;
    @JsonProperty("status")
    private String status;
    @JsonProperty("statusDateTime")
    private String statusDateTime;
}
