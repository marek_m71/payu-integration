package com.integration.payu.model.payu.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RefundCreateRequestPayU {
    @JsonProperty("refund")
    private RefundCreateRequestInfoTypePayU refund;
}
