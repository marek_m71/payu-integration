package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderCreateResponsePayU {
    @JsonProperty("redirectUri")
    private String redirectUri;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("extOrderId")
    private String extOrderId;
    @JsonProperty("status")
    private OrderCreateResponseStatusPayU status;
}
