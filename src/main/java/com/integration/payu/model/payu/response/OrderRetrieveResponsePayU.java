package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class OrderRetrieveResponsePayU {
    @JsonProperty("orders")
    private List<OrderRetrieveResponseOrderPayU> orders = null;
    @JsonProperty("status")
    private OrderRetrieveResponseStatusPayU status;
}
