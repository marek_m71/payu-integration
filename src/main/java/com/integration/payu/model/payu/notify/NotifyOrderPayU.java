package com.integration.payu.model.payu.notify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class NotifyOrderPayU {
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("extOrderId")
    private String extOrderId;
    @JsonProperty("orderCreateDate")
    private String orderCreateDate;
    @JsonProperty("notifyUrl")
    private String notifyUrl;
    @JsonProperty("customerIp")
    private String customerIp;
    @JsonProperty("merchantPosId")
    private String merchantPosId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("totalAmount")
    private String totalAmount;
    @JsonProperty("buyer")
    private NotifyBuyerPayU buyer;
    @JsonProperty("payMethod")
    private NotifyPayMethodPayU payMethod;
    @JsonProperty("products")
    private List<NotifyProductPayU> products = null;
    @JsonProperty("status")
    private String status;
}
