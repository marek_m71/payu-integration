package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderCreateResponseStatusPayU {
    @JsonProperty("statusCode")
    private String statusCode;
}
