package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RefundCreateResponsePayU {
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("refund")
    private RefundCreateResponseDetailsPayU refund;
    @JsonProperty("status")
    private RefundCreateResponseStatusPayU status;
}
