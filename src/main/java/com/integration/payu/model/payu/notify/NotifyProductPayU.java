package com.integration.payu.model.payu.notify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NotifyProductPayU {
    @JsonProperty("name")
    private String name;
    @JsonProperty("unitPrice")
    private String unitPrice;
    @JsonProperty("quantity")
    private String quantity;
}
