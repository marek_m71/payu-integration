package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class OrderRetrieveResponseOrderPayU {
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("extOrderId")
    private String extOrderId;
    @JsonProperty("orderCreateDate")
    private String orderCreateDate;
    @JsonProperty("notifyUrl")
    private String notifyUrl;
    @JsonProperty("customerIp")
    private String customerIp;
    @JsonProperty("merchantPosId")
    private String merchantPosId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("totalAmount")
    private String totalAmount;
    @JsonProperty("status")
    private String status;
    @JsonProperty("products")
    private List<OrderRetrieveResponseProductPayU> products = null;
}
