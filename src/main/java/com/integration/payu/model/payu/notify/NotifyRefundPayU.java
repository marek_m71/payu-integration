package com.integration.payu.model.payu.notify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NotifyRefundPayU {
    @JsonProperty("refundId")
    private String refundId;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("status")
    private String status;
    @JsonProperty("statusDateTime")
    private String statusDateTime;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("reasonDescription")
    private String reasonDescription;
    @JsonProperty("refundDate")
    private String refundDate;
}
