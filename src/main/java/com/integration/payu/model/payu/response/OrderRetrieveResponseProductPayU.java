package com.integration.payu.model.payu.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderRetrieveResponseProductPayU {
    @JsonProperty("name")
    private String name;
    @JsonProperty("unitPrice")
    private String unitPrice;
    @JsonProperty("quantity")
    private String quantity;
}
