package com.integration.payu.model.payu.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderCreateRequestProductPayU {
    @JsonProperty("name")
    private String name;
    @JsonProperty("unitPrice")
    private String unitPrice;
    @JsonProperty("quantity")
    private String quantity;
}
