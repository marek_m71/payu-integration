package com.integration.payu.model.payu.notify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NotifyPropertyPayU {
    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private String value;
}
