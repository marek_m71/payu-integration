package com.integration.payu.exception.handler;

import com.integration.payu.exception.advices.*;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
class ExceptionHandling implements ProblemHandling, FeignClientErrorAdviceTrait,
        HttpClientErrorAdviceTrait, RefundAdviceTrait, RefundNotFoundAdviceTrait,
        OrderNotFoundAdviceTrait, UserNotFoundAdviceTrait {
}

