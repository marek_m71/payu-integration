package com.integration.payu.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Long userId) {
        super("User with id: " + userId + " doesn't exist!");
    }

    public UserNotFoundException(String username) {
        super("User with username: " + username + " doesn't exist!");
    }
}
