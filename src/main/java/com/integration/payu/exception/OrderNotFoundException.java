package com.integration.payu.exception;

public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException(String orderId) {
        super("Order with id: " + orderId + " doesn't exist!");
    }
}
