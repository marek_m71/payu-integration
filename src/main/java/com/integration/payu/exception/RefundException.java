package com.integration.payu.exception;

public class RefundException extends RuntimeException {
    public RefundException(String message) {
        super(message);
    }
}
