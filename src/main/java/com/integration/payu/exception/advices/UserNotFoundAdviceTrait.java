package com.integration.payu.exception.advices;

import com.integration.payu.exception.UserNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.AdviceTrait;

public interface UserNotFoundAdviceTrait extends AdviceTrait {
    @ExceptionHandler
    default ResponseEntity<Problem> handleUserNotFoundException(final UserNotFoundException e, final NativeWebRequest request) {
        return create(Status.NOT_FOUND, e, request);
    }
}
