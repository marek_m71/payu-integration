package com.integration.payu.exception.advices;

final class AdviceTraitConstants {
    private AdviceTraitConstants() {
        // class for constant values
    }
    static final String PAYU = "PayU-Response";
}
