package com.integration.payu.exception.advices;

import com.integration.payu.exception.OrderNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.AdviceTrait;

public interface OrderNotFoundAdviceTrait extends AdviceTrait {
    @ExceptionHandler
    default ResponseEntity<Problem> handleOrderNotFoundException(final OrderNotFoundException e, final NativeWebRequest request) {
        return create(Status.NOT_FOUND, e, request);
    }
}
