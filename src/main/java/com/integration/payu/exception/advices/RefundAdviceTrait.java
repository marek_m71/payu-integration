package com.integration.payu.exception.advices;

import com.integration.payu.exception.RefundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.AdviceTrait;

public interface RefundAdviceTrait extends AdviceTrait {

    @ExceptionHandler
    default ResponseEntity<Problem> refundAmountErrorException(final RefundException e, final NativeWebRequest request) {
        Logger logger = LoggerFactory.getLogger(FeignClientErrorAdviceTrait.class);

        if (logger.isErrorEnabled()) {
            logger.error("RefundException, message: {}", e.getMessage());
        }
        return create(Status.BAD_REQUEST, e, request);
    }
}
