package com.integration.payu.exception.advices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.spring.web.advice.AdviceTrait;
import org.zalando.problem.violations.ConstraintViolationProblem;
import org.zalando.problem.violations.Violation;

import java.util.Collections;

import static com.integration.payu.exception.advices.AdviceTraitConstants.PAYU;
import static org.zalando.problem.Status.BAD_REQUEST;

public interface HttpClientErrorAdviceTrait extends AdviceTrait {

    @ExceptionHandler
    default ResponseEntity<Problem> handleHttpClientErrorException(final HttpClientErrorException e, final NativeWebRequest request) {
        Logger logger = LoggerFactory.getLogger(HttpClientErrorAdviceTrait.class);

        if (logger.isErrorEnabled()) {
            logger.error("HttpClientErrorException, status: {}, message: {}", e.getRawStatusCode(), e.getResponseBodyAsString());
        }
        final Violation payUViolation = new Violation(PAYU, e.getResponseBodyAsString());
        final Problem problem = new ConstraintViolationProblem(ConstraintViolationProblem.TYPE, BAD_REQUEST, Collections.singletonList(payUViolation));

        return create(e, problem, request);
    }
}
