package com.integration.payu.exception;

public class RefundNotFoundException extends RuntimeException {
    public RefundNotFoundException(String refundId) {
        super("Refund with id: " + refundId + " doesn't exist!");
    }
}